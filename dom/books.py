__author__ = 'Adam'
#!/usr/bin/env python
# -*- coding=utf-8 -*-

import bookdb

s = """\
<html>
<body>
<h2>Hello World!</h2>
</body>
</html>
"""



def application(environ, start_response):
    response_body = s % (    )
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 4444, application)
    srv.serve_forever()